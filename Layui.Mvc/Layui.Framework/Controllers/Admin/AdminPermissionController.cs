﻿using Layui.Framework.Filters;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;

namespace Layui.Framework.Controllers.Admin
{
    /// <summary>
    /// 需要权限验证的控制器继承
    /// </summary>
    [TypeFilter(typeof(PermissionActionFilter))]
    public class AdminPermissionController: PublicAdminController
    {

    }
}
