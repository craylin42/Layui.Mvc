﻿using Layui.Core;
using Layui.Framework.Infrastructure;
using Layui.Framework.Security.Admin;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Routing;
using System;
using System.Collections.Generic;
using System.Text;

namespace Layui.Framework
{
    public static class HtmlHelperExtensions
    {
        /// <summary>
        /// 必填项目信号提醒
        /// </summary>
        /// <param name="htmlHelper"></param>
        /// <returns></returns>
        public static HtmlString RequiredSpan(this IHtmlHelper htmlHelper)
        { 
            return new HtmlString(@"<span class=required-span>*</span>");
        }

    }
}
