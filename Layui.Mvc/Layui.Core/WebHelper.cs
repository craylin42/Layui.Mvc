﻿using Layui.Core.Infrastructure;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace Layui.Core
{
    public class WebHelper : IWebHelper
    {
        private IHttpContextAccessor _httpContextAccessor;

        public WebHelper(IHttpContextAccessor httpContextAccessor)
        {
            this._httpContextAccessor = httpContextAccessor;
        }

        /// <summary>
        /// 获取客户端ip
        /// </summary>
        /// <returns></returns>
        public string GetIPAddress()
        {
            return _httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();
        }

        /// <summary>
        /// 获取当前请求的Url
        /// </summary>
        /// <returns></returns>
        public string GetUrl()
        {
            return _httpContextAccessor.HttpContext.Request.Path;
        }

        public string GetUrlReferrer()
        {
            return _httpContextAccessor.HttpContext.Request.Headers["Referer"];
        }
    }
}
