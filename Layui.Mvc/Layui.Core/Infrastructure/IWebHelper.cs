﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Layui.Core.Infrastructure
{
    public interface IWebHelper
    {
        /// <summary>
        /// 获取客户端ip
        /// </summary>
        /// <returns></returns>
        string GetIPAddress();

        /// <summary>
        /// 获取当前请求的Url
        /// </summary>
        /// <returns></returns>
        string GetUrl();

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        string GetUrlReferrer();
    }
}
