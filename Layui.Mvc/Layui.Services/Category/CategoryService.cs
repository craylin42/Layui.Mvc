﻿using System;
using System.Collections.Generic;
using System.Text;
using Layui.Entities;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;

namespace Layui.Services.Category
{
    public class CategoryService : ICategoryService
    {
        private static readonly string MODEL_KEY = "Layui.services.category";

        private IMemoryCache _memoryCache;
        private LayuiDbContext _LayuiDbContext;

        public CategoryService(IMemoryCache memoryCache,
            LayuiDbContext LayuiDbContext)
        {
            this._LayuiDbContext = LayuiDbContext;
            this._memoryCache = memoryCache;
        }

        /// <summary>
        /// 初始化保存方法
        /// </summary>
        /// <param name="list"></param>
        public void InitCategory(List<Entities.Category> list)
        {
            var oldList = _LayuiDbContext.Categories.ToList();
            oldList.ForEach(del =>
            {
                var item = list.FirstOrDefault(o => o.SysResource == del.SysResource);
                if (item == null)
                {
                    var permissionList = _LayuiDbContext.SysPermissions.Where(o => o.CategoryId == del.Id).ToList();
                    _LayuiDbContext.SysPermissions.RemoveRange(permissionList);
                    _LayuiDbContext.Categories.Remove(del);
                }
            });
            list.ForEach(entity =>
            {
                var item = oldList.FirstOrDefault(o => o.SysResource == entity.SysResource);
                if (item == null)
                {
                    _LayuiDbContext.Categories.Add(entity);
                }
                else
                {
                    item.Action = entity.Action;
                    item.Controller = entity.Controller;
                    item.CssClass = entity.CssClass;
                    item.IsMenu = entity.IsMenu;
                    item.Name = entity.Name;
                    item.RouteName = entity.RouteName;
                    item.SysResource = entity.SysResource;
                    item.Sort = entity.Sort;
                    item.FatherID = entity.FatherID;
                    item.ResouceID = entity.ResouceID;
                }
            });
            _LayuiDbContext.SaveChanges();
            _memoryCache.Remove(MODEL_KEY);
        }

        /// <summary>
        /// 获取所有的菜单数据 并缓存
        /// </summary>
        /// <returns></returns>
        public List<Entities.Category> GetAllCache()
        {
            List<Entities.Category> list = null;
            if (!_memoryCache.TryGetValue<List<Entities.Category>>(MODEL_KEY, out list))
            {
                list = _LayuiDbContext.Categories.ToList();
                _memoryCache.Set(MODEL_KEY, list, DateTimeOffset.Now.AddDays(1));
            }
            return list;
        }
    }
}
