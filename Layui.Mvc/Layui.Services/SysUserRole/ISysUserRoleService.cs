﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Layui.Services.SysUserRole
{
    public interface ISysUserRoleService
    {
        /// <summary>
        /// 获取所有的数据
        /// </summary>
        /// <returns></returns>
        List<Entities.SysUserRole> GetAll();

        /// <summary>
        /// 移除缓存
        /// </summary>
        void removeCache();


        /// <summary>
        /// 保存设置的用户角色数据
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="roleIds"></param>
        void SaveUserRole(Guid userId, List<Guid> roleIds);
    }
}
