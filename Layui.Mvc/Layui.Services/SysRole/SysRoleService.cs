﻿using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Layui.Entities;
using Layui.Services.SysPermission;
using Layui.Services.SysUserRole;
using Microsoft.EntityFrameworkCore;

namespace Layui.Services.SysRole
{
    public class SysRoleService : ISysRoleService
    {
        private const string MODEL_KEY = "Layui.services.role_all";
        private IMemoryCache _memoryCache;
        private LayuiDbContext _LayuiDbContext;
        private ISysPermissionService _sysPermissionServices;
        private ISysUserRoleService _sysUserRoleService;

        public SysRoleService(IMemoryCache memoryCache,
             ISysUserRoleService sysUserRoleService,
             LayuiDbContext LayuiDbContext,
             ISysPermissionService sysPermissionServices)
        {
            this._LayuiDbContext = LayuiDbContext;
            this._sysUserRoleService = sysUserRoleService;
            this._sysPermissionServices = sysPermissionServices;
            this._memoryCache = memoryCache;
        }

        /// <summary>
        /// 删除角色
        /// </summary>
        /// <param name="roleId"></param>
        public void DeleteRole(Guid roleId)
        {
            var item = _LayuiDbContext.SysRoles.Include(x => x.SysPermissions).Include(x => x.SysUserRoles).FirstOrDefault(o => o.Id == roleId);
            if (item == null)
                return;
            _LayuiDbContext.SysPermissions.RemoveRange(item.SysPermissions.ToList());
            _LayuiDbContext.SysUserRoles.RemoveRange(item.SysUserRoles.ToList());
            _LayuiDbContext.SysRoles.Remove(item);
            _LayuiDbContext.SaveChanges();
            _memoryCache.Remove(MODEL_KEY);
            _sysPermissionServices.RemoveCache();
            _sysUserRoleService.removeCache();
        }

        /// <summary>
        /// 获取所有的roles数据
        /// 并缓存
        /// </summary>
        /// <returns></returns>
        public List<Entities.SysRole> GetAllRoles()
        {
            List<Entities.SysRole> list = null;
            _memoryCache.TryGetValue<List<Entities.SysRole>>(MODEL_KEY, out list);
            if (list != null)
                return list;
            list = _LayuiDbContext.SysRoles.ToList();
            _memoryCache.Set(MODEL_KEY, list, DateTimeOffset.Now.AddDays(1));
            return list;
        }


        /// <summary>
        /// 新增角色
        /// </summary>
        /// <param name="role"></param>
        public void InserRole(Entities.SysRole role)
        {
            _LayuiDbContext.SysRoles.Add(role);
            _LayuiDbContext.SaveChanges();
            _memoryCache.Remove(MODEL_KEY);
        }

        /// <summary>
        /// 修改角色
        /// </summary>
        /// <param name="role"></param>
        public void UpdateRole(Entities.SysRole role)
        {
            var entry = _LayuiDbContext.Entry(role);
            entry.State = EntityState.Unchanged;
            entry.Property("Name").IsModified = true;
            entry.Property("ModifiedTime").IsModified = true;
            entry.Property("Modifier").IsModified = true;
            _LayuiDbContext.SaveChanges();
            _memoryCache.Remove(MODEL_KEY);
        }

        /// <summary>
        /// 获取角色详情
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Entities.SysRole GetRole(Guid id)
        {
            return _LayuiDbContext.SysRoles.Find(id);
        }


    }
}
