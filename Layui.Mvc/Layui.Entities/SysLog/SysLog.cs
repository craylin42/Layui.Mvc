﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Layui.Entities
{
    [Table("SysLog")]
    public partial class SysLog
    {
        /// <summary>
        /// 
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int Level { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ShortMessage { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string FullMessage { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string IpAddress { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string PageUrl { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ReferrerUrl { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime CreationTime { get; set; }
    }
}
