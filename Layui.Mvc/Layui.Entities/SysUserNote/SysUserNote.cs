﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Layui.Entities
{
    [Table("SysUserNote")]
    public class SysUserNote: BaseEntity
    {
        /// <summary>
        /// id
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// 用户
        /// </summary>
        public Guid UserId { get; set; }

        /// <summary>
        /// 记录内容
        /// </summary>
        public string MsgContent { get; set; }

        /// <summary>
        /// 创建人
        /// </summary>
        public Guid Creator { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [ForeignKey("UserId")]
        public SysUser User { get; set; }

        [ForeignKey("Creator")]
        public SysUser CreatorUser { get; set; }
    }


}
