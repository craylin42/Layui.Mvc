﻿
using Layui.Entities;
using Layui.Framework.Controllers.Admin;
using Layui.Framework.Datatable;
using Layui.Services.SysLog;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Layui.Mvc.Areas.Admin.Controllers
{
    /// <summary>
    /// 系统日志
    /// </summary>
    [Route("admin/syslog")]
    public class SysLogController : AdminPermissionController
    {
        // GET: SysLog
        private ISysLogService _sysLogService;

        public SysLogController(ISysLogService sysLogService)
        {
            this._sysLogService = sysLogService;
        }
        [Route("",Name = "sysLogIndex")] 
        public ActionResult SysLogIndex(LogSearchArg arg,int page = 1,int size = 20)
        {
            var pageList = _sysLogService.Search(arg, page, size);
            var dataSource = pageList.ToDataSourceResult(arg);
            return View(dataSource);
        }

        /// <summary>
        /// 清理所有日志
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("clear", Name = "clearSysLog")]
        public JsonResult ClearSysLog()
        {
            _sysLogService.ClearLog();
            AjaxData.Status = true;
            AjaxData.Message = "清理成功";
            return Json(AjaxData);
        }



    }
}