﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Layui.Framework.Controllers.Admin;
using Layui.Services.Category;
using Layui.Framework.Menu;
using System.IO;
using Layui.Core;
using Microsoft.Extensions.Caching.Memory;
using Layui.Framework.Filters;

namespace Layui.Mvc.Areas.Admin.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("admin/category")]
    [NotPermissionFilter]
    public class CategoryController : AdminPermissionController
    {
        private ICategoryService _categoryService;

        public CategoryController(ICategoryService categoryService)
        { 
            this._categoryService = categoryService;
        }

        /// <summary>
        /// 功能菜单列表
        /// </summary>
        /// <returns></returns>
        [Route("", Name = "categoryIndex")]
        public IActionResult CategoryIndex()
        { 
            var list = _categoryService.GetAllCache();
            return View(list);
        }

        /// <summary>
        /// 初始化
        /// </summary>
        /// <returns></returns>
        [Route("init",Name = "initCategory")]
        public IActionResult InitCategory()
        {
            var xmlSiteMap = new XmlSiteMap();
            xmlSiteMap.LoadFrom(Path.Combine(Directory.GetCurrentDirectory(), "sitemap.xml"));
            List<Entities.Category> list = new List<Entities.Category>();
            xmlSiteMap.SiteMapNodes.ForEach(item =>
            {
                list.Add(new Entities.Category()
                {
                    Action = item.Action,
                    Controller = item.Controller,
                    CssClass = item.IconClass,
                    IsMenu = item.IsMenu,
                    Name = item.Name,
                    RouteName = item.RouteName,
                    SysResource = item.SysResource,
                    Sort = item.Sort,
                    FatherID = item.FatherID,
                    IsDisabled = false,
                    ResouceID = item.ResouceID
                });
            });
            _categoryService.InitCategory(list);
            AjaxData.Status = true;
            AjaxData.Message = "初始化成功";
            return Json(AjaxData);
        }

    }
}