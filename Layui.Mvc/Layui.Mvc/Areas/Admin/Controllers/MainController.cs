﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Layui.Framework.Controllers.Admin;
using Layui.Framework.Security.Admin;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.Cookies;
using System.Security.Claims;
using Layui.Core.Infrastructure;

namespace Layui.Mvc.Areas.Admin.Controllers
{ 
    [Route("admin/main")]
    public class MainController : PublicAdminController
    {
        private IAdminAuthService _adminAuthService;

        public MainController(IAdminAuthService adminAuthService)
        {
            this._adminAuthService = adminAuthService;
        }
         
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Route("",Name ="mainIndex")]
        public IActionResult Index()
        {
           var user = _adminAuthService.GetCurrentUser();

            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Route("out",Name ="signOut")]
        public IActionResult SignOut()
        {
            _adminAuthService.SignOut();
            return RedirectToRoute("adminLogin");
        }

    }
}