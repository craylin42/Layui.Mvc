﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Layui.Mvc.Areas.Admin.Models
{
    public class UserRoleViewModel
    {
        public UserRoleViewModel()
        {
            RoleList = new List<Entities.SysRole>();
            UserRoleList = new List<Entities.SysUserRole>();
        }
        /// <summary>
        /// 当前用户信息
        /// </summary>
        public Entities.SysUser SysUser { get; set; }

        /// <summary>
        /// 所有的角色
        /// </summary>
        public List<Entities.SysRole> RoleList { get; set; }

        /// <summary>
        /// 用户所属角色
        /// </summary>
        public List<Entities.SysUserRole> UserRoleList { get; set; }
    }
}
