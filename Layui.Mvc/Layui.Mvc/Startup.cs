﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Layui.Entities;
using Microsoft.EntityFrameworkCore;
using Layui.Services.Category;
using Layui.Core;
using Layui.Core.Extensions;
using Layui.Services.Setting;
using Layui.Core.Librs;
using Layui.Framework;
using Layui.Framework.Infrastructure;
using Layui.Framework.Security.Admin;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.Extensions.Caching.Memory;
using System.IO;
using Layui.Framework.Menu;
using Layui.Core.Infrastructure;
using Microsoft.AspNetCore.HttpOverrides;
using Layui.Framework.Middleware;
using Layui.Framework.Filters;
using Layui.Framework.Options;

namespace Layui.Mvc
{
    public class Startup
    {

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc()
                .AddJsonOptions(options => options.SerializerSettings.ContractResolver = new Newtonsoft.Json.Serialization.DefaultContractResolver());

            //
            services.AddDbContextPool<LayuiDbContext>(options => options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddAuthentication(o =>
            {
                o.DefaultAuthenticateScheme = CookieAdminAuthInfo.AuthenticationScheme;
                o.DefaultChallengeScheme = CookieAdminAuthInfo.AuthenticationScheme;
            })
                .AddCookie(CookieAdminAuthInfo.AuthenticationScheme, o =>
            {
                o.Cookie.Domain = "";
                o.Cookie.HttpOnly = true;
                o.Cookie.Path = "";
                o.LoginPath = "/admin/login";
            });

            //程序集依赖注入
            services.AddAssembly("Layui.Services");

            services.AddScoped<IWorkContext, WorkContext>();
            services.AddScoped<IAdminAuthService, AdminAuthService>();
            services.AddSingleton<IWebHelper, WebHelper>();
            services.AddMemoryCache();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();


            services.Configure<CustomOptions>(Configuration.GetSection("Custom"));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {  
            app.UseStaticFiles();

            app.UseAuthentication();
            app.UseDeveloperExceptionPage();
            //
            //app.UseSession();
            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                  name: "areas",
                  template: "{area:exists}/{controller=Login}/{action=Index}/{id?}"
                );
            });
        }
    }
}
